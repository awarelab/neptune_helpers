import json
import pandas as pd
import logging
import neptune
import os
import multiprocessing as mp
import time


log = logging.getLogger(__name__)

# This is ugly, but is needed since multiprocessing does not support passing
# lambdas
def get_channel_names_fun(name):
    if name == "cl_filter_fun":
        from cl_analyzer import cl_filter
        return cl_filter

    return None


def _get_experiments_data(api_token, organization,
                          project_name, experiments_ids,
                          channels_names,
                          parameters_to_add,
                          drop_x_channel,
                          final_values_only):
    dfs = []
    session = neptune.Session.with_default_backend(api_token=api_token)
    projects = session.get_projects(organization)
    project = projects[project_name]

    experiments = project.get_experiments(id=experiments_ids)

    for experiment in experiments:

        if "_fun" in channels_names:
            channel_names_to_get \
                = get_channel_names_fun(channels_names)(experiment)
        else:
            channel_names_to_get = channels_names

        try:
            values = experiment.get_numeric_channels_values(*channel_names_to_get)
        except KeyError as e:
            print("error downloading data from:", experiment, " skipping")
            print("Exception:", e)
            continue
        values['experiment_id'] = experiment.id
        for parameter in parameters_to_add:
            p = experiment.get_parameters()[parameter]
            try:
                p = float(p)
            except:
                pass

            values[parameter] = p

        if drop_x_channel:
            values.drop(rf"x", axis=1, inplace=True)

        if final_values_only:
            values = values[-1:]
        dfs.append(values)

    return dfs


class NeptuneHelper(object):
    def __init__(self, organization, project_name,
                 api_token=None, pool_size=20):
        print("Creating neptune_helper:", organization, "/", project_name)
        self.api_token = api_token
        if api_token is None:
            self.api_token = self.get_api_token()

        self.session = neptune.Session.with_default_backend(api_token=self.api_token)
        self.organization = organization
        self.project_name = organization + "/" + project_name

        self.projects = self.session.get_projects(self.organization)
        self.project = self.projects[self.project_name]
        self.pool_size = pool_size
        self._created_experiments = []

    @staticmethod
    def get_api_token():
        if "NEPTUNE_API_TOKEN" in os.environ:
            return os.environ["NEPTUNE_API_TOKEN"]
        elif "NEPTUNE_API_TOKEN_" in os.environ:
            return os.environ["NEPTUNE_API_TOKEN_"]
        else:
            log.error("NEPTUNE_API_TOKEN not found")
            exit(-1)

    def get_experiments(self, id=None, tag=None):
        return self.project.get_experiments(id=id, tag=tag)

    def create_experiment(self, name, params, tags):
        experiment = self.project.create_experiment(name=name,
                                                    params=params, tags=tags)
        self._created_experiments.append(experiment)
        return experiment

    def gc(self):
        for experiment in self._created_experiments:
            experiment.stop()
        self._created_experiments = []

    def get_parameters_for_experiments(self, tags, prune=False):
        dfs = []
        if type(tags) is str:
            tags = [tags]
        for tag in tags:
            experiments = self.project.get_experiments(tag=tag)
            for experiment in experiments:
                df = experiment.get_parameters()
                df['experiment_id'] = experiment.id
                df = pd.DataFrame.from_dict([df])
                dfs.append(df)
            df = pd.concat(dfs)
        if prune:
            c = [c for c in df.columns if len(set(df[c])) != 1]
            df = df[c]
        df.set_index('experiment_id', inplace=True)

        return df

    def get_channel_for_experiments(self, tag, channels_names,
                                    final_values_only=False,
                                    drop_x_channel=False,
                                    parameters_to_add=[]):
        experiments = self.project.get_experiments(tag=tag)
        experiments_ids = [experiment.id for experiment in experiments]
        if channels_names is '_numeric':
            channels = experiments[0].get_channels()

            channels_names = [channel_name for channel_name in channels if
             channels[channel_name]['channelType'] == 'numeric']

        start_time = time.time()

        if self.pool_size == 1:
            args = [self.api_token, self.organization,
                    self.project_name, experiments_ids,
                    channels_names, parameters_to_add,
                    drop_x_channel, final_values_only]

            dfs = _get_experiments_data(*args)
        else:
            num_experiments = len(experiments_ids)
            chunk_size = max(int(num_experiments/self.pool_size), 2)
            argssplit = [[self.api_token, self.organization,
                          self.project_name, experiments_ids[x:x+chunk_size],
                          channels_names, parameters_to_add,
                          drop_x_channel, final_values_only]
                         for x in range(0, num_experiments, chunk_size)]

            with mp.Pool(self.pool_size) as p:

                data = p.starmap(_get_experiments_data, argssplit)

                dfs = []
                for d in data:
                    dfs.extend(d)

        print("Time:", time.time() - start_time)

        return pd.concat(dfs, ignore_index=True)

    def experiments_to_json(self, tag, channel_name, parameters, json_path=None, db_metric_name=None, extra_columns=None):
        df = self.get_channel_for_experiments(
            tag, channel_name, final_values_only=True, parameters_to_add=parameters, drop_x_channel=True
        )
        if db_metric_name is not None:
            df = df.rename({channel_name: db_metric_name}, axis='columns')
        neptune_url_format = 'https://app.neptune.ml/{}/e/{}'.format(
            self.project_name, '{}'
        )
        df['neptune'] = df['experiment_id'].map(neptune_url_format.format)
        del df['experiment_id']
        if extra_columns is not None:
            for (key, value) in extra_columns.items():
                df[key] = value
        data = df.to_dict(orient='records')
        if json_path is not None:
            with open(json_path, 'w') as f:
                json.dump(data, f, indent=2)
        return data
