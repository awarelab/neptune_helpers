from neptune_helper import NeptuneHelper

import os

helper = NeptuneHelper(organization="pmtest",
                       project_name="noise-channel",
                       pool_size=1,
                       api_token="eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vdWkubmVwdHVuZS5haSIsImFwaV91cmwiOiJodHRwczovL3VpLm5lcHR1bmUuYWkiLCJhcGlfa2V5IjoiMDQ2OTZkMzItNGYzOC00YWU2LWIwNzktNzE0MzhjNjM0MjNmIn0=")

experiment_tags = ["nostalgic_lovelace"]
experiment_tags = ["fervent_kowalevski"]
experiment_tags = ["peaceful_allen"]
experiment_tags = ["thirsty_colden"]
experiment_tags = ["thirsty_cray"]
experiment_tags = ["competent_cori"]
experiment_tags = ["pedantic_hermann"]
experiment_tags = ["cranky_brattain"]
experiment_tags = ["peaceful_bell"]
experiment_tags = ["focused_brattain"]




pwd="/Users/piotrmilos/Dropbox/Applications/Overleaf/Noisy_channel ICML"


def get_data_for_tag(experiment_tag):

    experiment = helper.project.\
        get_experiments(tag=[experiment_tag, 'analysis', 'summary'])

    experiment[0].download_artifacts(destination_dir=pwd)
    os.chdir(pwd)
    os.system("unzip -o output.zip")


for experiment_tag in experiment_tags:
    get_data_for_tag(experiment_tag)
