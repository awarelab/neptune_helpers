import pathlib
import pandas as pd
import numpy as np
from neptune_helper import NeptuneHelper

_neptune_helper = None

def mean_if_not_to_many_nans(values, threshold=0.8):
    if np.count_nonzero(~np.isnan(values)) >= len(values)*threshold:
        return np.mean(values)
    else:
        return np.nan


def std_if_not_to_many_nans(values, threshold=0.8):
    if np.count_nonzero(~np.isnan(values)) >= len(values)*threshold:
        return np.std(values)
    else:
        return np.nan


def analyzer_factory(experiment_tag, relevant_parameter, project, storage_path):
    analyzer_class_ = None
    if project == "noise-channel":
        # This is to avoid circular imports
        from noise_analyzer import NoiseAnalyzer
        analyzer_class_ = NoiseAnalyzer

    if project == "bayesian-exploration":
        from bayes_analyzer import BayesAnalyzer
        analyzer_class_ = BayesAnalyzer

    if project == 'continual-learning':
        from cl_analyzer import CL_Analyzer
        analyzer_class_ = CL_Analyzer

    assert analyzer_class_ is not None, "Project unknown"

    return analyzer_class_(experiment_tag=experiment_tag,
                           relevant_parameter=relevant_parameter,
                           project=project,
                           storage_path=storage_path)


class Analyzer:

    def __init__(self, experiment_tag, relevant_parameter,
                 organization="pmtest", project=None,
                 storage_path=None,
                 invalidate_storage=False,
                 channels_to_get='_numeric'):

        self._experiment_tag = experiment_tag
        self._relevant_parameter = relevant_parameter
        self._organization = organization
        self._project = project
        if storage_path:
            self._storage_path = \
                pathlib.Path(storage_path) / self._experiment_tag
            self._storage_path.mkdir(parents=True, exist_ok=True)
        else:
            self._storage_path = None
        self._invalidate_storage = invalidate_storage
        self._relevant_parameter = relevant_parameter
        self._experiment_data = None
        self._channels_to_get = channels_to_get

    def _get_path(self, artifact_name):
        return self._storage_path / artifact_name \
            if self._storage_path else None

    @property
    def neptune_helper(self):
        global _neptune_helper
        if _neptune_helper is None:
            _neptune_helper = NeptuneHelper(self._organization, self._project,
                                            api_token="eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vdWkubmVwdHVuZS5haSIsImFwaV91cmwiOiJodHRwczovL3VpLm5lcHR1bmUuYWkiLCJhcGlfa2V5IjoiMDQ2OTZkMzItNGYzOC00YWU2LWIwNzktNzE0MzhjNjM0MjNmIn0=")
        return _neptune_helper

    def gc(self):
        self.neptune_helper.gc()

    @property
    def experiment_data(self):
        if self._experiment_data is not None:
            return self._experiment_data

        path = self._get_path(rf'experiment_data_{self._experiment_tag}.pkl')
        # Otherwise check in the internal storage
        if path and not self._invalidate_storage and path.exists():
            self._experiment_data = pd.read_pickle(path)

            return self._experiment_data

        self._experiment_data = self.neptune_helper.\
            get_channel_for_experiments(self._experiment_tag,
                                        self._channels_to_get,
                                        final_values_only=False,
                                        parameters_to_add=[self._relevant_parameter],
                                        drop_x_channel=False)

        if path:
            self._experiment_data.to_pickle(path)

        return self._experiment_data

    def dump_means_to_neptune(self):
        if len(self.neptune_helper.get_experiments
                   (tag=[self._experiment_tag, 'analysis'])) != 0:
            # already done
            return

        relevant_parameter = self._relevant_parameter
        _df = self.experiment_data.groupby([relevant_parameter, 'x']).agg(
            mean_if_not_to_many_nans).reset_index()
        relevant_parameters = pd.unique(_df[relevant_parameter])
        for parameter_value in relevant_parameters:
            _df_for_parameter = _df[_df[relevant_parameter] == parameter_value]
            _df_for_parameter = _df_for_parameter.drop(relevant_parameter, axis=1)
            _df_for_parameter = _df_for_parameter.set_index('x').sort_index()

            experiment = \
                self.neptune_helper.create_experiment(
                    name=rf"analysis_experiment_{parameter_value}",
                    params={relevant_parameter: parameter_value},
                    tags=[self._experiment_tag, 'analysis'])

            for idx, data in _df_for_parameter.iterrows():
                for name, value in data.dropna().iteritems():
                    channel_name = name  # [0] + names[name[1]]
                    experiment.log_metric(channel_name, x=idx, y=value)

    def gc(self):
        self.neptune_helper.gc()
