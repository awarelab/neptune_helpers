import re
from itertools import combinations
from neptunecontrib.api import log_table
import io
import matplotlib.pyplot as plt
from scipy import stats
import seaborn as sns
import numpy as np
import pandas as pd

sns.set_style("whitegrid")

from analyzer import Analyzer


class BayesAnalyzer(Analyzer):

    def __init__(self, experiment_tag, relevant_parameter,
                 organization="pmtest", project=None,
                 storage_path=None,
                 invalidate_storage=False):

        super().__init__(experiment_tag, relevant_parameter,
                 organization, project, storage_path, invalidate_storage)

    def summary_experiment(self, thresholds=[3e6 - 5e4, 1e6, 2e5]):
        if len(self.neptune_helper.get_experiments
                   (tag=[self._experiment_tag, 'analysis', 'summary'])) != 0:
            # already done
            return

        experiment = \
            self.neptune_helper.create_experiment(
                name=rf"analysis_experiment",
                params={self._relevant_parameter: 'summary'},
                tags=[self._experiment_tag, 'analysis', 'summary'])

        relevant_pramater_values \
            = self.experiment_data[self._relevant_parameter].unique()
        __data = self.experiment_data[
            ['x', 'experiment_id', 'AverageTestEpRet', self._relevant_parameter]]

        data = []
        for param_val in relevant_pramater_values:

            for threshold in thresholds:
                _data = __data.copy()
                _data = _data[_data[self._relevant_parameter] == param_val]

                end_x = threshold
                start_x = end_x - 5e5

                _data = _data[(_data.x >= start_x) & (_data.x <= end_x)]
                _data_means = _data.groupby('experiment_id').mean()

                mean = _data_means['AverageTestEpRet'].mean()
                std = _data_means['AverageTestEpRet'].std()
                data.append([param_val, threshold, mean, std])

                fig, _ = plt.subplots()
                g_ = sns.violinplot(x="AverageTestEpRet", data=_data_means)
                g_.set_title(param_val + "_" + str(threshold))
                experiment.log_image("violin plots", fig)

        data_table = pd.DataFrame(data,
                                  columns=[self._relevant_parameter,
                                           'steps', 'mean', 'std']).round(0)

        log_table(self._experiment_tag , data_table, experiment=experiment)
