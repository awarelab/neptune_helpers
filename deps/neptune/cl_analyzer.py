from analyzer import analyzer_factory, Analyzer


def filter_substring(hays, *needles):
    filtered_list = [hay for hay in hays if all(needle in hay for needle in needles)]
    return sorted(filtered_list)


def cl_filter(experiment):
    channels = experiment.get_channels()
    channels_names = [channel_name for channel_name in channels if
                      channels[channel_name]['channelType'] == 'numeric']

    metric_name = filter_substring(channels_names, f'test/stochastic/', 'success')[
        0]

    channels_names = [metric_name]

    return channels_names


class CL_Analyzer(Analyzer):

    def __init__(self, experiment_tag, relevant_parameter,
                 organization="pmtest", project=None,
                 storage_path=None,
                 invalidate_storage=False):

        super().__init__(experiment_tag, relevant_parameter,
                 organization, project, storage_path, invalidate_storage,
                         "cl_filter_fun")






experiment_tag = 'mw_251_pretty_mashup_pairs'
relevant_parameter = 'task_list'
project = 'continual-learning'
storage_path = '/tmp'

analyzer = analyzer_factory(experiment_tag, relevant_parameter, project,
                                    storage_path)

abc = analyzer.experiment_data
print(abc.columns)
