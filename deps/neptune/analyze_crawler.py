import argparse
import json
import time
import sys, traceback
from analyzer import analyzer_factory


def process_experiment(experiment_tag, relevant_parameter,
                       project, storage_path):

    start_time = time.time()
    print("Processing:", experiment_tag)

    try:
        analyzer = analyzer_factory(experiment_tag, relevant_parameter, project,
                                    storage_path)
        analyzer.dump_means_to_neptune()
        analyzer.summary_experiment()
        analyzer.gc()
    except Exception as e:
        print(rf"Processing failed due to, ", e)
        traceback.print_exc(file=sys.stdout)

    print("Processing:", experiment['experiment_tag'], "done in ",
          time.time() - start_time)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("experiments_spec", type=str)
    parser.add_argument("project", type=str)
    parser.add_argument("storage_path", type=str)
    args = parser.parse_args()

    with open(args.experiments_spec) as f:
        experiments = json.load(f)

    print(experiments)

    for experiment in experiments:
        process_experiment(**experiment,
                           project=args.project, storage_path=args.storage_path)

