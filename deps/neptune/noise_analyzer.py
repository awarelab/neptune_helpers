import re
from itertools import combinations

from neptune.internal.utils.image import _get_figure_as_image
from neptunecontrib.api import log_table
import io
import matplotlib.pyplot as plt
from scipy import stats
import seaborn as sns
import numpy as np

sns.set_style("whitegrid")

from analyzer import Analyzer


class NoiseAnalyzer(Analyzer):

    def __init__(self, experiment_tag, relevant_parameter,
                 violin_pattern=".*in_sample.*",
                 pair_pattern=".*in_sample.*|.*in_train.*",
                 table_pattern="context independence in_sample|conflict count_OneMsgPerClass in_sample|topographic similarity in_sample",
                 reg_prune="samples|acc_color|acc_shape|eval_test_in_sample_acc_all",
                 organization="pmtest", project=None,
                 storage_path=None,
                 invalidate_storage=False):


        super().__init__(experiment_tag, relevant_parameter,
                 organization, project, storage_path, invalidate_storage)

        self._violin_pattern = violin_pattern
        self._pair_pattern = pair_pattern
        self._reg_prune = reg_prune
        self._table_pattern = table_pattern


    @property
    def metrics(self):
        pattern = self._violin_pattern + "|" + self._pair_pattern
        l = list(self.experiment_data.columns)
        cols_to_keep = [name for name in l if re.search(pattern, name)]

        results = self.experiment_data.copy()
        results_ = results[
            cols_to_keep + ['experiment_id', self._relevant_parameter]].dropna()

        results_ = results_.groupby('experiment_id').tail(20).groupby(
            ['experiment_id', self._relevant_parameter]).mean()
        results_ = results_.round(5)
        results_ = results_.reset_index()

        return results_

    @property
    def _violin_channels(self):
        l = list(self.experiment_data.columns)
        violin_channels = \
            [name for name in l if re.search(self._violin_pattern, name)]

        violin_channels = [name for name in violin_channels if
                           not re.search(self._reg_prune, name)]

        return violin_channels

    def _violin_plots(self, metric_results, experiment, suffix=""):
        for channel in self._violin_channels:
            fig, _ = plt.subplots()
            g_ = sns.violinplot(y=channel, x=self._relevant_parameter,
                                data=metric_results)
            g_.set_title(channel)

            self._log_image(experiment, "violin plots " + suffix, channel, fig)
            #
            # with open("/tmp/_tmp.png", "wb") as f:
            #     fig.savefig(f, format='png', bbox_inches="tight")
            #
            # experiment.log_image("violin plots " + suffix, "/tmp/_tmp.png")
            # experiment.log_artifact("/tmp/_tmp.png", "images/violin plots " + channel + suffix + ".png")

    @property
    def _tables_channels(self):
        l = list(self.experiment_data.columns)
        table_channels = \
            [name for name in l if re.search(self._table_pattern, name)]

        table_channels = [name for name in table_channels if
                          not re.search(self._reg_prune, name)]

        return table_channels

    def _print_tables(self, metric_results, experiment, suffix=""):
        def normalized_std(values):
            return np.std(values) / len(values)

        # print("%Latex for experiment:", self._experiment_tag,
        #       " with threshold ", suffix, "="*30)
        _df = metric_results[self._tables_channels + [self._relevant_parameter]]
        _df = _df.groupby(self._relevant_parameter).agg(
            ['mean', normalized_std]).round(3)

        str_ = _df.to_latex()
        dest = self._experiment_tag + "_" + suffix + ".txt"
        experiment.log_artifact(io.StringIO(str_), destination=dest)

        log_table(self._experiment_tag + '_' + suffix, _df,
                  experiment=experiment)

    @staticmethod
    def _correlation_str(metric_results, ch_1, ch_2):
        try:

            res = stats.spearmanr(
                metric_results[ch_1],
                metric_results[ch_2])

            res_str = str(res)

            res = stats.pearsonr(
                metric_results[ch_1],
                metric_results[ch_2])

            res_str += '\n'
            res_str += rf"Correlation{res}"

            return res_str
        except ValueError:
            return "Correlations could not be computed"

    def _log_image(self, experiment, channel_name, image_name, fig, description=None):
        with open("/tmp/_tmp.png", "wb") as f:
            fig.savefig(f, format='png', bbox_inches="tight")

        experiment.log_image(channel_name, "/tmp/_tmp.png",
                             description=description)
        path = "images/" + self._experiment_tag + "/" + channel_name + image_name
        # to avoid problem with latex
        path = path.replace("_", "")
        path = path.replace(".", "")
        path = path.replace(" ", "")
        path += ".png"

        experiment.log_artifact("/tmp/_tmp.png", path)


    def _pair_plots(self, metric_results, experiment, suffix=""):

        for ch_1, ch_2 in combinations(self._violin_channels, 2):
            fig, _ = plt.subplots()
            desc = self._correlation_str(metric_results, ch_1, ch_2)
            sns.regplot(data=metric_results, x=ch_1, y=ch_2)
            self._log_image(experiment, "pair plots " + suffix,
                            ch_1 + ch_2, fig, desc)

        for ch_1 in self._violin_channels:
            ch_2 = ch_1.replace('in_sample', 'in_train')
            fig, _ = plt.subplots()
            desc = self._correlation_str(metric_results, ch_1, ch_2)
            sns.scatterplot(data=metric_results, x=ch_1, y=ch_2)
            self._log_image(experiment, "pair plots train test " + suffix,
                            ch_1, fig, desc)
            #
            # experiment.log_image("pair plots train test " + suffix,
            #                      fig, description=desc)

    def summary_experiment(self, thresholds=[0.0, 0.95]):
        if len(self.neptune_helper.get_experiments
                   (tag=[self._experiment_tag, 'analysis', 'summary'])) != 0:
            # already done
            return

        experiment = \
            self.neptune_helper.create_experiment(
                name=rf"analysis_experiment",
                params={self._relevant_parameter: 'summary'},
                tags=[self._experiment_tag, 'analysis', 'summary'])

        for threshold in thresholds:
            metric_results = self.metrics
            metric_results = metric_results[
                metric_results['eval_test_in_train_acc'] > threshold]
            if len(metric_results) == 0:
                continue

            self._violin_plots(metric_results, experiment, str(threshold))
            self._pair_plots(metric_results, experiment, str(threshold))
            self._print_tables(metric_results, experiment, str(threshold))

