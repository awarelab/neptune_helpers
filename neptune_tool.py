"""
Example of usage

export neptune_tool_storage=/tmp
export neptune_tool_prometheus=plgloss@pro.cyfronet.pl

python neptune_tools.py https://ui.neptune.ml/loss/poloplus/experiments?tag=%5B%22vigilant_mahavira%22%5D

python neptune_tools.py https://ui.neptune.ml/loss/poloplus/e/POL-3071/details

"""


import argparse
import logging
import os
from enum import Enum

from neptune_helper import NeptuneHelper, parse_link

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class Cluster(Enum):
    PROMETHEUS = 0
    EAGLE = 1
    OTHER = 2


def infer_cluster(path):
    if "plggluna" in path:
        return Cluster.PROMETHEUS
    if "lustre" in path:
        return Cluster.EAGLE

    return Cluster.OTHER


def _get_host_name(cluster):
    if cluster == Cluster.PROMETHEUS:
        host_name = os.environ.get("neptune_tool_prometheus", None)
        if host_name is None:
            log.warning("set neptune_tool_prometheus env variable to ssh config. eg. plgloss@pro.cyfronet.pl")
            exit(1)
        else:
            return host_name

    if cluster == Cluster.EAGLE:
        host_name = os.environ.get("neptune_tool_eagle", None)
        if host_name is None:
            log.warning("set neptune_tool_prometheus env variable to ssh config. eg. plgloss@eagle.man.poznan.pl")
            exit(1)
        else:
            return host_name

    log.warning("Unknown host")
    exit(1)


def infer_experiment_path(experiment):
    params_to_hold_pwd = ["pwd", "log_dir"]  # Weirdness as it depends on the project
    params = experiment.get_properties()
    for param_pwd in params_to_hold_pwd:
        if param_pwd in params:
            path = params[param_pwd]
            cluster = infer_cluster(path)
            log.info(f"Detected:{path} on {cluster}")
            return path, cluster

    log.warning("Experiment path not found")
    exit(1)


def download_artifacts(experiment):
    artifacts_paths = ["agent_eval_videos", "videos", "traces"]
    experiment_path, cluster = infer_experiment_path(experiment)
    cluster_host_name = _get_host_name(cluster)
    neptune_tool_storage = os.environ.get("neptune_tool_storage", ".")
    path_to_dump = os.path.join(neptune_tool_storage, experiment.id)
    os.makedirs(path_to_dump, exist_ok=True)
    for artifacts_path in artifacts_paths:
        rsync_command = f'rsync  -avzhe ssh "{cluster_host_name}:{experiment_path}/{artifacts_path}" {path_to_dump}'
        log.info(f"Executing:{rsync_command}")
        os.system(rsync_command)
        post_command = os.environ.get("neptune_tool_post_command", None)
        if post_command is not None:
            os.system(f"{post_command} {path_to_dump}")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("neptune_url", type=str)
    args = parser.parse_args()
    link_data = parse_link(args.neptune_url)
    log.info(f"link_data:{link_data}")
    organization, project, id, is_tag_link = link_data

    helper = NeptuneHelper(organization, project)
    if is_tag_link:
        experiments = helper.get_experiments(tag=id)
    else:
        experiments = helper.get_experiments(id=id)
    log.info(f"Found experiments:{experiments}")

    for experiment in experiments:
        download_artifacts(experiment)


if __name__ == "__main__":
    main()